﻿using EsunSplit.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace EsunSplit.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            var viewModel = new ViewModel()
            {
                Groups = CommonInfo.Groups,
                PaymentInfos = CommonInfo.PaymentInfos
            };
            ViewData["flag"] = CommonInfo.Flag2;

            return View(viewModel);
        }
        public IActionResult Insert(string title, string amount)
        {
            var viewModel = new ViewModel()
            {
                insertData = new InsertData() { title = title, amount = amount },
            };
            return View(viewModel);
        }
        public IActionResult Insert2(string title, string amount)
        {
            title = "計程車";
            amount = "240";
            var viewModel = new ViewModel()
            {
                insertData = new InsertData() { title = title, amount = amount },
            };
            return View(viewModel);
        }
        public IActionResult GetLineID()
        {
            return View();
        }

        public IActionResult Detail()
        {
            var viewModel = new ViewModel()
            {
                Groups = CommonInfo.Groups,
                PaymentInfos = CommonInfo.PaymentInfos
            };

            return View(viewModel);
        }
        [HttpPost]
        public IActionResult Line([FromBody] dynamic data)
        {
            var json = data.GetRawText();
            LineEventModel info = JsonConvert.DeserializeObject<LineEventModel>(json);
            string inputText = info?.events[0].message.text;
            if (inputText != null)
            {
                switch (inputText)
                {
                    case "/e":
                        LineHandler.GetMenu(info);
                        break;
                    case "/消費記錄":
                        LineHandler.GetDetail(info);
                        break;
                    case "/消費紀錄":
                        LineHandler.GetDetail(info);
                        break;

                }
            }
            return Json("");
        }

        [HttpGet]
        public IActionResult SendQRCode()
        {
            LineHandler.SendQRCode();
            return Json("");
        }

        [HttpGet]
        public IActionResult ChangeFlag()
        {
            CommonInfo.Flag2 = true;
            return Json("");
        }
    }

}
