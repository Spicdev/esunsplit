﻿using System.Collections.Generic;

namespace EsunSplit.Models
{
    public static class CommonInfo
    {
        public static List<Group> Groups { get; set; }
        public static bool Flag2 { get; set; }

        public static List<PaymentInfo> PaymentInfos { get; set; }

        public static SplitPayment SplitPayment { get; set; }

        static CommonInfo()
        {
            Flag2 = false;
            Groups = new List<Group>()
            {
                new Group("綠島浮潛團","+1500"),
                new Group("玉山午餐吃什麼","-99"),
                new Group("親家人明算帳","-2900")
            };

            PaymentInfos = new List<PaymentInfo>()
            {
                new PaymentInfo("綠島浮潛團","2020-06-05","火車票",2400,"+1600","-800","-800","吳小哲","魔術師小何","許玉兒"),
                new PaymentInfo("綠島浮潛團","2020-06-06","浮潛費用",7500,"-2500","+5000","-2500","吳小哲","魔術師小何","許玉兒"),
                new PaymentInfo("綠島浮潛團","2020-06-06","深海大鳳梨熱炒",1550,"+1033","-517","-516","吳小哲","魔術師小何","許玉兒"),
                new PaymentInfo("綠島浮潛團","2020-06-07","宵夜鹽酥雞",300,"0","-150","+150","吳小哲","魔術師小何","許玉兒"),
                new PaymentInfo("綠島浮潛團","2020-06-07","麥當勞早餐",219,"-80","+189","-109","吳小哲","魔術師小何","許玉兒")
            };

            SplitPayment = new SplitPayment("回程計程車", "2020-06-07", "420", "吳小哲", "魔術師小何", "許玉兒");
        }
    }

    public class Group
    {
        public Group(string groupName, string amount)
        {
            GroupName = groupName;
            Amount = amount;
        }
        public string GroupName { get; set; }

        public string Amount { get; set; }
    }

    public class PaymentInfo
    {
        public PaymentInfo(string groupName, string paymentTime, string title, int amount, string myAmount, string user1Amount, string user2Amount, string myName, string user1Name, string user2Name)
        {
            GroupName = groupName;
            PaymentTime = paymentTime;
            Title = title;
            Amount = amount;
            MyAmount = myAmount;
            User1Amount = user1Amount;
            User2Amount = user2Amount;
            MyName = myName;
            User1Name = user1Name;
            User2Name = user2Name;
        }

        public string GroupName { get; set; }

        public string PaymentTime { get; set; }

        public string Title { get; set; }

        public int Amount { get; set; }

        public string MyAmount { get; set; }

        public string User1Amount { get; set; }

        public string User2Amount { get; set; }

        public string MyName { get; set; }

        public string User1Name { get; set; }

        public string User2Name { get; set; }
    }

    public class SplitPayment
    {
        public SplitPayment(string title, string paymentTime, string amount, string myName, string user1Name, string user2Name)
        {
            PaymentTime = paymentTime;
            Title = title;
            Amount = amount;
            MyName = myName;
            User1Name = user1Name;
            User2Name = user2Name;
        }
        public string Title { get; set; }

        public string PaymentTime { get; set; }

        public string Amount { get; set; }

        public string MyName { get; set; }

        public string User1Name { get; set; }

        public string User2Name { get; set; }
    }
}
