﻿using System.Collections.Generic;

namespace EsunSplit.Models
{
    public class LineEventModel
    {
        public IList<Events> events { get; set; }
        public string destination { get; set; }

    }
    public class Events
    {
        public string type { get; set; }
        public string replyToken { get; set; }
        public Source source { get; set; }
        public string timestamp { get; set; }
        public string mode { get; set; }
        public Message message { get; set; }

    }
    public class Source
    {
        public string userId { get; set; }
        public string type { get; set; }

    }
    public class Message
    {
        public string type { get; set; }
        public string id { get; set; }
        public string text { get; set; }

    }
    
}
