﻿using System.Collections.Generic;

namespace EsunSplit.Models
{
    public class LineTextModel
    {
        public string replyToken { get; set; }
        public IList<Messages> messages { get; set; }

    }
    public class Messages
    {
        public string type { get; set; }
        public string text { get; set; }

    }
}