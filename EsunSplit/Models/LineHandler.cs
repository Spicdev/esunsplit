﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;

namespace EsunSplit.Models
{
    public class LineHandler
    {
        private static readonly string CHANNEL_ACCESS_TOKEN = "FdROyjEen9Ks0KYlI+pUmwn+TOOlD/WqblYKMZy34kfyz68zcapQnL6XcmO1IyYTfoiep1mQubl3/jDdjbSyTlmIfKuHTdmwFBlq1FJ/LBeC+fbJl+AZYEtKrHM3eNmN5Kw3I7YPnkyLjrzi9MinyAdB04t89/1O/w1cDnyilFU=";

        public static void SendQRCode()
        {
            string text = @"
{
    ""to"": ""Ub2537581cfa211580186b0082c800c03"",
    ""messages"": [{
        ""type"": ""flex"",
        ""altText"": ""請款通知"",
        ""contents"": {
            ""type"": ""bubble"",
            ""hero"": {
                ""type"": ""image"",
                ""url"": ""https://www.shoptiq.com.sg/sites/default/files/iStock-628883362%20%281%29.jpg"",
                ""size"": ""full"",
                ""aspectRatio"": ""20:13"",
                ""aspectMode"": ""cover"",
                ""action"": {
                    ""type"": ""uri"",
                    ""uri"": ""http://linecorp.com/""
                }
            },
            ""body"": {
                ""type"": ""box"",
                ""layout"": ""vertical"",
                ""spacing"": ""md"",
                ""contents"": [{
                        ""type"": ""text"",
                        ""text"": ""請款資訊"",
                        ""wrap"": true,
                        ""weight"": ""bold"",
                        ""gravity"": ""center"",
                        ""size"": ""xl""
                    },
                    {
                        ""type"": ""box"",
                        ""layout"": ""vertical"",
                        ""margin"": ""lg"",
                        ""spacing"": ""sm"",
                        ""contents"": [{
                            ""type"": ""box"",
                            ""layout"": ""baseline"",
                            ""spacing"": ""sm"",
                            ""contents"": [{
                                    ""type"": ""text"",
                                    ""text"": ""收款人："",
                                    ""color"": ""#aaaaaa"",
                                    ""size"": ""sm"",
                                    ""flex"": 3
                                },
                                {
                                    ""type"": ""text"",
                                    ""text"": ""何小霆"",
                                    ""wrap"": true,
                                    ""size"": ""sm"",
                                    ""color"": ""#666666"",
                                    ""flex"": 4
                                }
                            ]
                        }]
                    },
                    {
                        ""type"": ""box"",
                        ""layout"": ""vertical"",
                        ""margin"": ""lg"",
                        ""spacing"": ""sm"",
                        ""contents"": [{
                            ""type"": ""box"",
                            ""layout"": ""baseline"",
                            ""spacing"": ""sm"",
                            ""contents"": [{
                                    ""type"": ""text"",
                                    ""text"": ""付款人："",
                                    ""color"": ""#aaaaaa"",
                                    ""size"": ""sm"",
                                    ""flex"": 3
                                },
                                {
                                    ""type"": ""text"",
                                    ""text"": ""吳小哲"",
                                    ""wrap"": true,
                                    ""size"": ""sm"",
                                    ""color"": ""#666666"",
                                    ""flex"": 4
                                }
                            ]
                        }]
                    },
                    {
                        ""type"": ""box"",
                        ""layout"": ""vertical"",
                        ""margin"": ""lg"",
                        ""spacing"": ""sm"",
                        ""contents"": [{
                            ""type"": ""box"",
                            ""layout"": ""baseline"",
                            ""spacing"": ""sm"",
                            ""contents"": [{
                                    ""type"": ""text"",
                                    ""text"": ""金額："",
                                    ""color"": ""#aaaaaa"",
                                    ""size"": ""sm"",
                                    ""flex"": 3
                                },
                                {
                                    ""type"": ""text"",
                                    ""text"": ""TWD$ 980"",
                                    ""wrap"": true,
                                    ""size"": ""sm"",
                                    ""color"": ""#666666"",
                                    ""flex"": 4
                                }
                            ]
                        }]
                    },
                    {
                        ""type"": ""box"",
                        ""layout"": ""vertical"",
                        ""margin"": ""xxl"",
                        ""contents"": [{
                                ""type"": ""spacer""
                            },
                            {
                                ""type"": ""image"",
                                ""url"": ""https://scdn.line-apps.com/n/channel_devcenter/img/fx/linecorp_code_withborder.png"",
                                ""aspectMode"": ""cover"",
                                ""size"": ""xl""
                            }
                        ]
                    }
                ]
            }
        }
    }]
}
";
            Push(text);
        }

        public static void GetMenu(LineEventModel info)
        {
            string text1 = @"
{
    ""replyToken"":" + @"""" + info.events[0].replyToken + @"""" + @",
    ""messages"": [{
        ""type"": ""template"",
        ""altText"": ""this is a buttons template"",
        ""template"": {
            ""type"": ""buttons"",
            ""actions"": [{
                    ""type"": ""uri"",
                    ""label"": ""查看分帳明細"",
                    ""uri"": ""https://liff.line.me/1654362030-yeJKnKO2""
                },
                {
                    ""type"": ""uri"",
                    ""label"": ""新增一筆紀錄"",
                    ""uri"": ""https://liff.line.me/1654362030-GVqxJxrV""
                }
            ],
            ""thumbnailImageUrl"": ""https://calhoun.edu/wp-content/uploads/2018/05/Accounting-Feature.jpg"",
            ""title"": ""e 速分小幫手"",
            ""text"": ""您想做些什麼呢？""
        }
    }]
}
";
            Send(text1);
        }

        public static void GetDetail(LineEventModel info)
        {
            string text1 = @"
{
    ""replyToken"":" + @"""" + info.events[0].replyToken + @"""" + @",
    ""messages"": [{
        ""type"": ""flex"",
        ""altText"": ""消費資訊"",
        ""contents"": {
            ""type"": ""bubble"",
            ""body"": {
                ""type"": ""box"",
                ""layout"": ""vertical"",
                ""contents"": [{
                        ""type"": ""text"",
                        ""text"": ""綠島浮潛團"",
                        ""weight"": ""bold"",
                        ""color"": ""#1DB446"",
                        ""size"": ""sm""
                    },
                    {
                        ""type"": ""text"",
                        ""text"": ""消費資訊"",
                        ""weight"": ""bold"",
                        ""size"": ""xxl"",
                        ""margin"": ""md""
                    },
                    {
                        ""type"": ""separator"",
                        ""margin"": ""xxl""
                    },
                    {
                        ""type"": ""box"",
                        ""layout"": ""vertical"",
                        ""margin"": ""xxl"",
                        ""spacing"": ""sm"",
                        ""contents"": [{
                                ""type"": ""box"",
                                ""layout"": ""horizontal"",
                                ""contents"": [{
                                        ""type"": ""text"",
                                        ""text"": ""2020-06-05"",
                                        ""size"": ""sm"",
                                        ""color"": ""#555555"",
                                        ""flex"": 0
                                    },
                                    {
                                        ""type"": ""text"",
                                        ""text"": ""火車票"",
                                        ""size"": ""sm"",
                                        ""color"": ""#111111"",
                                        ""align"": ""center"",
                                        ""flex"": 8
                                    },
                                    {
                                        ""type"": ""text"",
                                        ""text"": ""$2400"",
                                        ""flex"": 7,
                                        ""size"": ""sm"",
                                        ""align"": ""end""
                                    }
                                ]
                            },
                            {
                                ""type"": ""box"",
                                ""layout"": ""horizontal"",
                                ""contents"": [{
                                        ""type"": ""text"",
                                        ""text"": ""2020-06-06"",
                                        ""size"": ""sm"",
                                        ""color"": ""#555555"",
                                        ""flex"": 0
                                    },
                                    {
                                        ""type"": ""text"",
                                        ""text"": ""浮潛費用"",
                                        ""size"": ""sm"",
                                        ""color"": ""#111111"",
                                        ""align"": ""center"",
                                        ""flex"": 8
                                    },
                                    {
                                        ""type"": ""text"",
                                        ""text"": ""$7500"",
                                        ""flex"": 7,
                                        ""size"": ""sm"",
                                        ""align"": ""end""
                                    }
                                ]
                            },
                            {
                                ""type"": ""box"",
                                ""layout"": ""horizontal"",
                                ""contents"": [{
                                        ""type"": ""text"",
                                        ""text"": ""2020-06-06"",
                                        ""size"": ""sm"",
                                        ""color"": ""#555555"",
                                        ""flex"": 0
                                    },
                                    {
                                        ""type"": ""text"",
                                        ""text"": ""深海大鳳梨熱炒"",
                                        ""size"": ""sm"",
                                        ""color"": ""#111111"",
                                        ""align"": ""center"",
                                        ""flex"": 8
                                    },
                                    {
                                        ""type"": ""text"",
                                        ""text"": ""$1550"",
                                        ""flex"": 7,
                                        ""size"": ""sm"",
                                        ""align"": ""end""
                                    }
                                ]
                            },
                            {
                                ""type"": ""box"",
                                ""layout"": ""horizontal"",
                                ""contents"": [{
                                        ""type"": ""text"",
                                        ""text"": ""2020-06-07"",
                                        ""size"": ""sm"",
                                        ""color"": ""#555555"",
                                        ""flex"": 0
                                    },
                                    {
                                        ""type"": ""text"",
                                        ""text"": ""宵夜鹽酥雞"",
                                        ""size"": ""sm"",
                                        ""color"": ""#111111"",
                                        ""align"": ""center"",
                                        ""flex"": 8
                                    },
                                    {
                                        ""type"": ""text"",
                                        ""text"": ""$300"",
                                        ""flex"": 7,
                                        ""size"": ""sm"",
                                        ""align"": ""end""
                                    }
                                ]
                            },
                            {
                                ""type"": ""box"",
                                ""layout"": ""horizontal"",
                                ""contents"": [{
                                        ""type"": ""text"",
                                        ""text"": ""2020-06-07"",
                                        ""size"": ""sm"",
                                        ""color"": ""#555555"",
                                        ""flex"": 0
                                    },
                                    {
                                        ""type"": ""text"",
                                        ""text"": ""麥當勞早餐"",
                                        ""size"": ""sm"",
                                        ""color"": ""#111111"",
                                        ""align"": ""center"",
                                        ""flex"": 8
                                    },
                                    {
                                        ""type"": ""text"",
                                        ""text"": ""$219"",
                                        ""flex"": 7,
                                        ""size"": ""sm"",
                                        ""align"": ""end""
                                    }
                                ]
                            }
                        ]
                    }
                ]
            },
            ""footer"": {
                ""type"": ""box"",
                ""layout"": ""vertical"",
                ""contents"": [{
                    ""type"": ""button"",
                    ""action"": {
                        ""type"": ""uri"",
                        ""label"": ""查看詳細資訊"",
                        ""uri"": ""https://liff.line.me/1654362030-yeJKnKO2""
                    }
                }]
            },
            ""styles"": {
                ""footer"": {
                    ""separator"": true
                }
            }
        }
    }]
}
";
            string text2 = @"
{
    ""replyToken"":" + @"""" + info.events[0].replyToken + @"""" + @",
    ""messages"": [{
        ""type"": ""flex"",
        ""altText"": ""消費資訊"",
        ""contents"": {
            ""type"": ""bubble"",
            ""body"": {
                ""type"": ""box"",
                ""layout"": ""vertical"",
                ""contents"": [{
                        ""type"": ""text"",
                        ""text"": ""綠島浮潛團"",
                        ""weight"": ""bold"",
                        ""color"": ""#1DB446"",
                        ""size"": ""sm""
                    },
                    {
                        ""type"": ""text"",
                        ""text"": ""消費資訊"",
                        ""weight"": ""bold"",
                        ""size"": ""xxl"",
                        ""margin"": ""md""
                    },
                    {
                        ""type"": ""separator"",
                        ""margin"": ""xxl""
                    },
                    {
                        ""type"": ""box"",
                        ""layout"": ""vertical"",
                        ""margin"": ""xxl"",
                        ""spacing"": ""sm"",
                        ""contents"": [{
                                ""type"": ""box"",
                                ""layout"": ""horizontal"",
                                ""contents"": [{
                                        ""type"": ""text"",
                                        ""text"": ""2020-06-05"",
                                        ""size"": ""sm"",
                                        ""color"": ""#555555"",
                                        ""flex"": 0
                                    },
                                    {
                                        ""type"": ""text"",
                                        ""text"": ""火車票"",
                                        ""size"": ""sm"",
                                        ""color"": ""#111111"",
                                        ""align"": ""center"",
                                        ""flex"": 8
                                    },
                                    {
                                        ""type"": ""text"",
                                        ""text"": ""$2400"",
                                        ""flex"": 7,
                                        ""size"": ""sm"",
                                        ""align"": ""end""
                                    }
                                ]
                            },
                            {
                                ""type"": ""box"",
                                ""layout"": ""horizontal"",
                                ""contents"": [{
                                        ""type"": ""text"",
                                        ""text"": ""2020-06-06"",
                                        ""size"": ""sm"",
                                        ""color"": ""#555555"",
                                        ""flex"": 0
                                    },
                                    {
                                        ""type"": ""text"",
                                        ""text"": ""浮潛費用"",
                                        ""size"": ""sm"",
                                        ""color"": ""#111111"",
                                        ""align"": ""center"",
                                        ""flex"": 8
                                    },
                                    {
                                        ""type"": ""text"",
                                        ""text"": ""$7500"",
                                        ""flex"": 7,
                                        ""size"": ""sm"",
                                        ""align"": ""end""
                                    }
                                ]
                            },
                            {
                                ""type"": ""box"",
                                ""layout"": ""horizontal"",
                                ""contents"": [{
                                        ""type"": ""text"",
                                        ""text"": ""2020-06-06"",
                                        ""size"": ""sm"",
                                        ""color"": ""#555555"",
                                        ""flex"": 0
                                    },
                                    {
                                        ""type"": ""text"",
                                        ""text"": ""深海大鳳梨熱炒"",
                                        ""size"": ""sm"",
                                        ""color"": ""#111111"",
                                        ""align"": ""center"",
                                        ""flex"": 8
                                    },
                                    {
                                        ""type"": ""text"",
                                        ""text"": ""$1550"",
                                        ""flex"": 7,
                                        ""size"": ""sm"",
                                        ""align"": ""end""
                                    }
                                ]
                            },
                            {
                                ""type"": ""box"",
                                ""layout"": ""horizontal"",
                                ""contents"": [{
                                        ""type"": ""text"",
                                        ""text"": ""2020-06-07"",
                                        ""size"": ""sm"",
                                        ""color"": ""#555555"",
                                        ""flex"": 0
                                    },
                                    {
                                        ""type"": ""text"",
                                        ""text"": ""宵夜鹽酥雞"",
                                        ""size"": ""sm"",
                                        ""color"": ""#111111"",
                                        ""align"": ""center"",
                                        ""flex"": 8
                                    },
                                    {
                                        ""type"": ""text"",
                                        ""text"": ""$300"",
                                        ""flex"": 7,
                                        ""size"": ""sm"",
                                        ""align"": ""end""
                                    }
                                ]
                            },
                            {
                                ""type"": ""box"",
                                ""layout"": ""horizontal"",
                                ""contents"": [{
                                        ""type"": ""text"",
                                        ""text"": ""2020-06-07"",
                                        ""size"": ""sm"",
                                        ""color"": ""#555555"",
                                        ""flex"": 0
                                    },
                                    {
                                        ""type"": ""text"",
                                        ""text"": ""麥當勞早餐"",
                                        ""size"": ""sm"",
                                        ""color"": ""#111111"",
                                        ""align"": ""center"",
                                        ""flex"": 8
                                    },
                                    {
                                        ""type"": ""text"",
                                        ""text"": ""$219"",
                                        ""flex"": 7,
                                        ""size"": ""sm"",
                                        ""align"": ""end""
                                    }
                                ]
                            },
                            {
                                ""type"": ""box"",
                                ""layout"": ""horizontal"",
                                ""contents"": [{
                                        ""type"": ""text"",
                                        ""text"": ""2020-06-19"",
                                        ""size"": ""sm"",
                                        ""color"": ""#555555"",
                                        ""flex"": 0
                                    },
                                    {
                                        ""type"": ""text"",
                                        ""text"": ""計程車"",
                                        ""size"": ""sm"",
                                        ""color"": ""#111111"",
                                        ""align"": ""center"",
                                        ""flex"": 8
                                    },
                                    {
                                        ""type"": ""text"",
                                        ""text"": ""$240"",
                                        ""flex"": 7,
                                        ""size"": ""sm"",
                                        ""align"": ""end""
                                    }
                                ]
                            }
                        ]
                    }
                ]
            },
            ""footer"": {
                ""type"": ""box"",
                ""layout"": ""vertical"",
                ""contents"": [{
                    ""type"": ""button"",
                    ""action"": {
                        ""type"": ""uri"",
                        ""label"": ""查看詳細資訊"",
                        ""uri"": ""https://liff.line.me/1654362030-yeJKnKO2""
                    }
                }]
            },
            ""styles"": {
                ""footer"": {
                    ""separator"": true
                }
            }
        }
    }]
}
";
            if (!CommonInfo.Flag2) {
                Send(text1);
            }
            else
            {
                Send(text2);
            }
        }

        public static void Send(string text)
        {
            HttpContent httpContent = new StringContent(text);
            httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            httpContent.Headers.ContentType.CharSet = "utf-8";
            using (HttpClient httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", $"{CHANNEL_ACCESS_TOKEN}");
                HttpResponseMessage response = httpClient.PostAsync("https://api.line.me/v2/bot/message/reply", httpContent).Result;
            }
        }
        public static void Push(string text)
        {
            HttpContent httpContent = new StringContent(text);
            httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            httpContent.Headers.ContentType.CharSet = "utf-8";
            using (HttpClient httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", $"{CHANNEL_ACCESS_TOKEN}");
                HttpResponseMessage response = httpClient.PostAsync("https://api.line.me/v2/bot/message/push", httpContent).Result;
            }
        }
    }
}
