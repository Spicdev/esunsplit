﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EsunSplit.Models
{
    public class ViewModel
    {
        public  List<Group> Groups { get; set; }

        public List<PaymentInfo> PaymentInfos { get; set; }

        public InsertData insertData { get; set; }

    }

    public class InsertData
    {
        public string title { get; set; }
        public string amount { get; set; }
    }
}
